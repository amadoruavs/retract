#ifndef _RETRACT_H
#define _RETRACT_H

#include <zephyr.h>
#include <device.h>

enum gear_state {
	GEAR_INITIALIZATION,
	GEAR_DEPLOYING,
	GEAR_DEPLOYED,
	GEAR_RETRACTING,
	GEAR_RETRACTED
};

struct global_state {
	enum gear_state gear_state;
	
	// TODO: this isn't necessarily the best way to do things
	// but storing device pointers here is much faster than
	// initializing them in every out-of-context function
	struct device *limitr_dev;
	struct device *limitd_dev;
	struct device *dir1_dev;
	struct device *dir2_dev;
	struct device *en_dev;
};

char *gear_state_string(enum gear_state gear_state);

int gear_deploy();
int gear_retract();

#endif
