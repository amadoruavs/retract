#include <stdio.h>
#include <logging/log.h>

#include <zephyr.h>
#include <device.h>
#include <drivers/gpio.h>

#include "retract.h"

LOG_MODULE_REGISTER(gear, LOG_LEVEL_DBG);

#define GEAR_STACK_SIZE 1024
#define GEAR_PRIORITY 0

#define MOTOR_NODE DT_ALIAS(motor)
#if DT_NODE_HAS_STATUS(MOTOR_NODE, okay)
#define MOTOR_DIR1_LABEL DT_GPIO_LABEL_BY_IDX(MOTOR_NODE, direction_gpios, 0)
#define MOTOR_DIR1_PIN DT_GPIO_PIN_BY_IDX(MOTOR_NODE, direction_gpios, 0)
#define MOTOR_DIR1_FLAGS DT_GPIO_FLAGS_BY_IDX(MOTOR_NODE, direction_gpios, 0)
#define MOTOR_DIR2_LABEL DT_GPIO_LABEL_BY_IDX(MOTOR_NODE, direction_gpios, 1)
#define MOTOR_DIR2_PIN DT_GPIO_PIN_BY_IDX(MOTOR_NODE, direction_gpios, 1)
#define MOTOR_DIR2_FLAGS DT_GPIO_FLAGS_BY_IDX(MOTOR_NODE, direction_gpios, 1)
#define MOTOR_EN_LABEL DT_GPIO_LABEL(MOTOR_NODE, enable_gpios)
#define MOTOR_EN_PIN DT_GPIO_PIN(MOTOR_NODE, enable_gpios)
#define MOTOR_EN_FLAGS DT_GPIO_FLAGS(MOTOR_NODE, enable_gpios)
#else
#error "Unsupported board."
#endif

#define LIMITR_NODE DT_ALIAS(limitr)
#if DT_NODE_HAS_STATUS(LIMITR_NODE, okay)
#define LIMITR_LABEL DT_GPIO_LABEL(LIMITR_NODE, gpios)
#define LIMITR_PIN DT_GPIO_PIN(LIMITR_NODE, gpios)
#define LIMITR_FLAGS DT_GPIO_FLAGS(LIMITR_NODE, gpios)
#else
#error "Unsupported board"
#endif

#define LIMITD_NODE DT_ALIAS(limitd)
#if DT_NODE_HAS_STATUS(LIMITD_NODE, okay)
#define LIMITD_LABEL DT_GPIO_LABEL(LIMITD_NODE, gpios)
#define LIMITD_PIN DT_GPIO_PIN(LIMITD_NODE, gpios)
#define LIMITD_FLAGS DT_GPIO_FLAGS(LIMITD_NODE, gpios)
#else
#error "Unsupported board"
#endif

extern struct global_state gstate;

static void motor_deploy(const struct device *dir1,
		const struct device *dir2, const struct device *en)
{
	// deployment defined as DIR1 = HIGH and DIR2 = LOW
	gpio_pin_set(dir1, MOTOR_DIR1_PIN, 1);
	gpio_pin_set(dir2, MOTOR_DIR2_PIN, 0);
	// and just enable the motor (at full speed for now)
	gpio_pin_set(en, MOTOR_EN_PIN, 1);
}

static void motor_retract(const struct device *dir1,
		const struct device *dir2, const struct device *en)
{
	// retraction defined as DIR1 = LOW and DIR2 = HIGH
	gpio_pin_set(dir1, MOTOR_DIR1_PIN, 0);
	gpio_pin_set(dir2, MOTOR_DIR2_PIN, 1);
	// and just enable the motor (at full speed for now)
	gpio_pin_set(en, MOTOR_EN_PIN, 1);
}

static void motor_off(const struct device *en)
{
	gpio_pin_set(en, MOTOR_EN_PIN, 0);
}

int gear_deploy()
{
	int pin_state;
	pin_state = gpio_pin_get(gstate.limitd_dev, LIMITD_PIN);
	if (pin_state)
	{
		// already deployed
		return 1;
	}

	LOG_INF("Deploying...");
	gstate.gear_state = GEAR_DEPLOYING;
	motor_deploy(gstate.dir1_dev, gstate.dir2_dev, gstate.en_dev);

	return 0;
}

int gear_retract()
{
	int pin_state;
	pin_state = gpio_pin_get(gstate.limitr_dev, LIMITR_PIN);
	if (pin_state)
	{
		// already retracted
		return 1;
	}

	LOG_INF("Retracting...");
	gstate.gear_state = GEAR_RETRACTING;
	motor_retract(gstate.dir1_dev, gstate.dir2_dev, gstate.en_dev);

	return 0;
}

char *gear_state_string(enum gear_state gear_state)
{
    switch (gear_state)
    {
    case GEAR_INITIALIZATION:
        return "Initialization";
    case GEAR_DEPLOYING:
        return "Deploying";
    case GEAR_DEPLOYED:
        return "Deployed";
    case GEAR_RETRACTING:
        return "Retracting";
    case GEAR_RETRACTED:
        return "Retracted";
    default:
        return "Unknown";
    }
}

static void gear_thread(void *unused1, void *unused2, void *unused3)
{
	ARG_UNUSED(unused1);
	ARG_UNUSED(unused2);
	ARG_UNUSED(unused3);

	gstate.dir1_dev = device_get_binding(MOTOR_DIR1_LABEL);
	if (gstate.dir1_dev == NULL) {
		LOG_ERR("Unable to initialize device %s", MOTOR_DIR1_LABEL);
		return;
	}
	gstate.dir2_dev = device_get_binding(MOTOR_DIR2_LABEL);
	if (gstate.dir2_dev == NULL) {
		LOG_ERR("Unable to initialize device %s", MOTOR_DIR1_LABEL);
		return;
	}
	gstate.en_dev = device_get_binding(MOTOR_EN_LABEL);
	if (gstate.en_dev == NULL) {
		LOG_ERR("Unable to initialize device %s", MOTOR_EN_LABEL);
		return;
	}

	int ret;
	ret = gpio_pin_configure(gstate.dir1_dev, MOTOR_DIR1_PIN, GPIO_OUTPUT | MOTOR_DIR1_FLAGS);
	if (ret != 0) {
		LOG_ERR("Unable to configure %s pin %d: %d",
				MOTOR_DIR1_LABEL, MOTOR_DIR1_PIN, ret);
		return;
	}
	ret = gpio_pin_configure(gstate.dir2_dev, MOTOR_DIR2_PIN, GPIO_OUTPUT | MOTOR_DIR2_FLAGS);
	if (ret != 0) {
		LOG_ERR("Unable to configure %s pin %d: %d",
				MOTOR_DIR2_LABEL, MOTOR_DIR2_PIN, ret);
		return;
	}
	ret = gpio_pin_configure(gstate.en_dev, MOTOR_EN_PIN, GPIO_OUTPUT | MOTOR_EN_FLAGS);
	if (ret != 0) {
		LOG_ERR("Unable to configure %s pin %d: %d",
				MOTOR_EN_LABEL, MOTOR_EN_PIN, ret);
		return;
	}

	gstate.limitr_dev = device_get_binding(LIMITR_LABEL);
    if (gstate.limitr_dev == NULL) {
        LOG_ERR("Unable to initialize device %s", LIMITR_LABEL);
        return;
    }

    gstate.limitd_dev = device_get_binding(LIMITD_LABEL);
    if (gstate.limitd_dev == NULL) {
        LOG_ERR("Unable to initialize device %s", LIMITD_LABEL);
        return;
    }

    ret = gpio_pin_configure(gstate.limitr_dev, LIMITR_PIN, GPIO_INPUT | LIMITR_FLAGS);
    if (ret) {
        LOG_ERR("Unable to configure limitr pin");
        return;
    }
    ret = gpio_pin_configure(gstate.limitd_dev, LIMITD_PIN, GPIO_INPUT | LIMITD_FLAGS);
    if (ret) {
        LOG_ERR("Unable to configure limitd pin");
        return;
    }

	gstate.gear_state = GEAR_INITIALIZATION;

	// simple finite state machine
	while (1) {
		switch (gstate.gear_state)
		{
		case GEAR_INITIALIZATION:
		{
			int pin_state;
			pin_state = gpio_pin_get(gstate.limitd_dev, LIMITD_PIN);
			LOG_DBG("Initializing...");
			if (pin_state) {
				// detected initialization in deployed mode
				gstate.gear_state = GEAR_DEPLOYED;
				LOG_DBG("Detected deployed mode");
				break;
			}
			pin_state = gpio_pin_get(gstate.limitr_dev, LIMITR_PIN);
			if (pin_state) {
				// detected initialization in retracted mode
				gstate.gear_state = GEAR_RETRACTED;
				LOG_DBG("Detected retracted mode");
				break;
			}

			// undefined startup, so trigger deploy
			gstate.gear_state = GEAR_DEPLOYING;
			LOG_DBG("Unable to detect state, deploying");
			motor_deploy(gstate.dir1_dev, gstate.dir2_dev, gstate.en_dev);

			break;
		}
		case GEAR_RETRACTED:
		{
			break;
		}
		case GEAR_DEPLOYED:
		{
			break;
		}
		case GEAR_RETRACTING:
		{
			int pin_state;
			// check if we need to stop
			pin_state = gpio_pin_get(gstate.limitr_dev, LIMITR_PIN);
			if (pin_state) {
				LOG_DBG("Retraction finished");
				motor_off(gstate.en_dev);
				gstate.gear_state = GEAR_RETRACTED;
			}

			break;
		}
		case GEAR_DEPLOYING:
		{
			int pin_state;
			// check if we need to stop
			pin_state = gpio_pin_get(gstate.limitd_dev, LIMITD_PIN);
			if (pin_state) {
				LOG_DBG("Deployment finished");
				motor_off(gstate.en_dev);
				gstate.gear_state = GEAR_DEPLOYED;
			}

			break;
		}
		}

		k_sleep(K_MSEC(5)); /* 200 Hz */
	}
}

K_THREAD_DEFINE(gear_thread_id, GEAR_STACK_SIZE,
				gear_thread, NULL, NULL, NULL,
				GEAR_PRIORITY, 0, 0);
