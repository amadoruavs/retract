#include <stdio.h>
#include <logging/log.h>
#include <zephyr.h>
#include <string.h>
#include <usb/usb_device.h>
#include <drivers/uart.h>

#include "retract.h"

struct global_state gstate;

LOG_MODULE_REGISTER(retract_main, LOG_LEVEL_DBG);

void main(void)
{
	LOG_INF("Starting landing gear driver...");

	const struct device *dev = device_get_binding(CONFIG_UART_CONSOLE_ON_DEV_NAME);
	uint32_t dtr = 0;

	if (usb_enable(NULL)) {
		return;
	}

	while (!dtr) {
		uart_line_ctrl_get(dev, UART_LINE_CTRL_DTR, &dtr);
	}

	if (strlen(CONFIG_UART_CONSOLE_ON_DEV_NAME) !=
		strlen("CDC_ACM_0") ||
		strncmp(CONFIG_UART_CONSOLE_ON_DEV_NAME, "CDC_ACM_0",
			strlen(CONFIG_UART_CONSOLE_ON_DEV_NAME))) {
		LOG_ERR("Error: console device name is not USB CDC ACM");

		return;
	}

	/*while (1) {*/
		/*printf("Hello, world!\n");*/
		/*k_sleep(K_MSEC(500));*/
	/*}*/
	// gear monitor, console and interfacing threads run themselves
	// so we can return from main now
	/*return;*/
}
