#include <stdio.h>
#include <logging/log.h>

#include <zephyr.h>
#include <shell/shell.h>

#include "retract.h"

extern struct global_state gstate;

static int cmd_gear_status(const struct shell *shell,
		size_t argc, char **argv, void *data)
{
	shell_print(shell, "Current gear state: %s\n",
			gear_state_string(gstate.gear_state));
	
	return 0;
}

static int cmd_gear_deploy(const struct shell *shell,
		size_t argc, char **argv, void *data)
{
	int ret = gear_deploy();
	if (ret == 1) {
		shell_print(shell, "Gear already deployed!\n");
		return -EIO;
	}

	return 0;
}

static int cmd_gear_retract(const struct shell *shell,
		size_t argc, char **argv, void *data)
{
	int ret = gear_retract();
	if (ret == 1) {
		shell_print(shell, "Gear already retracted!\n");
		return -EIO;
	}

	return 0;
}

SHELL_STATIC_SUBCMD_SET_CREATE(sub_gear,
		SHELL_CMD(status, NULL, "Print gear status", cmd_gear_status),
		SHELL_CMD(deploy, NULL, "Deploy gear", cmd_gear_deploy),
		SHELL_CMD(retract, NULL, "Retract gear", cmd_gear_retract),
		SHELL_SUBCMD_SET_END
);

SHELL_CMD_REGISTER(gear, &sub_gear, "Gear control commands", NULL);
